
import java.io.*;
import java.util.*;

public class main {
    public static final String espions = "./excel\\Espions.csv";


    public static void main(String[] args) throws IOException {

        HashMap<Integer, String>  Listedesnoms = new HashMap<>();
        HashMap<Integer, String>   Listecontinent = new HashMap<>();
        HashMap<Integer, String>  Listelangues = new HashMap<>();
        //utilisation du fonction pour remplir les maps
        remplirHashmap(Listedesnoms,Listecontinent,Listelangues);

        //Supprime dans les liste les personne qui ne parle qu'une langue
        SupprimerLeslangues(Listelangues,Listedesnoms,Listecontinent);


        //Supprime dans les liste les personne qui n'ont pas voyager en europe
        SupprimerLescontinentvoyagés(Listecontinent,Listedesnoms,Listelangues);

        // affiche l'espion serguei
        System.out.print("\n \n");
        System.out.print("l'espion Serguei a trouvé été:");
        System.out.print("\n");
        System.out.print("le nom d'usage de serguei: "+Listedesnoms.get(trouverespions(Listedesnoms)));
        System.out.print("\n");
        System.out.print("les continent visité par serguei: "+Listecontinent.get(trouverespions(Listedesnoms)));
        System.out.print("\n");
        System.out.print("les langues parlés par serguei:  "+ Listelangues.get(trouverespions(Listedesnoms)));
        System.out.print("\n");

    }




    public static void SupprimerLeslangues(HashMap<Integer, String> listelangues,HashMap<Integer,String>listedesnoms,HashMap<Integer,String>Listecontinent) {

        ArrayList<Integer>leskeys=new ArrayList<>();

        for (Map.Entry<Integer,String> entry : listelangues.entrySet()) {
            String[] values = entry.getValue().split("/");
            if (values.length==1){
              leskeys.add(entry.getKey());
            }

        }
        for(int f=0;f<leskeys.size();f++){
            //a chaque occurence on retire dans tout les listes on supprime l'item liées a clés
            listedesnoms.remove(leskeys.get(f));
            Listecontinent.remove(leskeys.get(f));
            listelangues.remove(leskeys.get(f));
        }
        System.out.print("liste apres la suppression des  voyageur qui parle qu'une langue:"+"\n");
        afficheliste(listedesnoms,Listecontinent,listelangues);
        System.out.print("\n");

    }


    public static void SupprimerLescontinentvoyagés(HashMap<Integer, String> listedesContinents,HashMap<Integer,String>listedesnoms,HashMap<Integer,String>Listelangues) {

        ArrayList<Integer>leskeys=new ArrayList<>();

        for (Map.Entry<Integer,String> entry :listedesContinents.entrySet()) {
            String values = entry.getValue();
            boolean continenteurope=values.contains("Europe");

            if(!continenteurope){
                leskeys.add(entry.getKey());
            }
        }
        for (int i=0;i<leskeys.size();i++){
            //a chaque occurence on retire dans tout les listes on supprime l'item liées a clés
            listedesnoms.remove(leskeys.get(i));
            listedesContinents.remove(leskeys.get(i));
            Listelangues.remove(leskeys.get(i));
        }
        System.out.print("liste apres la suppression des  voyageur n'ont pas voyagés en europe:"+"\n");
        afficheliste(listedesnoms,listedesContinents,Listelangues);
        System.out.print("\n");

    }

    public static Integer  trouverespions(HashMap<Integer, String> listedesNoms) {
        /**
         *
         * parcourir les liste des noms et retourne la  clé pour trouver l espion
         * @return la clé pour trouver l'espions
         */

        Integer laclé=null;
          //parcourir la liste des noms
        for (Map.Entry<Integer,String> entry :listedesNoms.entrySet()) {
            //split la valeur du nom
            String[] values = entry.getValue().split(",");

            //prend en parametre le prenom d'un voyageur si correspond a serguei alors on retourne true
            if(rechercheprenomdelespion(values[0].toLowerCase())){
                //retourne la clé de la map qui permet de trouver l'entrée
                return entry.getKey();
            }


        }


        return laclé;
    }



    public static List<List<String>>readfilecsv(String File){
        /**
         * fonction qui permet de lire un fichier csv et retourner une liste par
         * return liste de lignes du fichier
         */

        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br =new BufferedReader(new FileReader(File))){
            String line;
            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");
                records.add(Arrays.asList(values));
            }
    } catch (IOException e) {
            throw new RuntimeException(e);
        }

         // retourne la liste
        return records;

    }

    public static boolean rechercheprenomdelespion(String prenom){
        /**
         * fonction qui permet de trouver l'espion par son prenom
         * @param prenom:le prenom d'un voyageur
         */
        boolean espiontrouve=false;
        char ch;
        //le prenom de l'espion
        String prenomespion="serguei";

        int nblettrescorrespondent=0;

        //boucle qui parcourir le prenom donné en parametre
        for (int i=0; i<prenom.length(); i++)
        {
            ch= prenom.charAt(i); //extrait chaque lettre du prenom

              //boucle qui parcoure le prenom de l'espions
              for (int f=0;f<prenomespion.length();f++){
                  //si la lettre du prenom  apparait dans le prenom de l'espion alors on incremente le nb lettres correspondent
                if(ch==prenomespion.charAt(f)){
                     nblettrescorrespondent++;
                   break;
                }
            }
        }

         //si le nb lettres correspondent est égale a la longueur de prenom de l'espion alors on a trouvé l'espions
        if(nblettrescorrespondent==prenomespion.length()){
            espiontrouve=true;
        }else{
            espiontrouve=false;
        }

        return espiontrouve;
    }


 public static void afficheliste(HashMap<Integer, String> listedesNoms,HashMap<Integer, String> listedescontinent,HashMap<Integer, String> listedeslangues){
     /**
      * fonction qui affiche la liste des voyageurs
      * @param listedesNoms:liste des noms  des voyageurs
      * @param listedescontinent:liste des continent visités des voyageurs
      *  @param listedeslangues:Liste des langues parlés par les voyageurs
      */


     //recupérer la liste des clé
        Set<Integer>keys=listedesNoms.keySet();
        for (Integer k:keys){
            System.out.print("\n");
            System.out.print("le nom: "+listedesNoms.get(k)+ "\n ses langues parlés: "+listedeslangues.get(k)+"\nles continent visité: "+listedescontinent.get(k));
            System.out.print("\n");
            System.out.print("*************************");
        }


 }

 public  static void remplirHashmap(  HashMap<Integer, String>  Listedesnoms,HashMap<Integer, String> Listecontinent,HashMap<Integer,String>Listelangues)
 {
     /**
      * fonction remplir les hashmap
      * @param listedesNoms:liste des noms  des voyageurs
      *  @param listedescontinent:liste des continent visités des voyageurs
      * @param listedeslangues:Liste des langues parlés par les voyageurs
      *
      *
      */
     List<List<String>> Lalistevoyagers = readfilecsv(espions);

     int i=0;
     for (List<String> uneligne : Lalistevoyagers) {
         i++;
         Listedesnoms.put(i,uneligne.get(1) + "," + uneligne.get(2));
         Listecontinent.put(i,uneligne.get(3));
         Listelangues.put(i,uneligne.get(4));

     }

 }



}


